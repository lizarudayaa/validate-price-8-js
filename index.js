let priceInput = document.querySelector("#price");
let span = document.createElement('span');
span.classList.add('priceValue');
let priceText = document.createElement("p");
priceInput.addEventListener('focus',() => {
    priceInput.style.border = " 5px solid white";
});

priceInput.addEventListener("blur", () => {

    if (priceInput.value < 0 || priceInput.value === "" ) {
        priceInput.style.border = " 5px solid black";
        document.querySelector("label").after(priceText);
        priceText.innerText = 'Введите точную цену';
        span.remove();
    } else {
        document.querySelector("label").before(span);
        span.innerHTML = `Текущая цена : ${priceInput.value} <button class="close">X</button>`;
        priceInput.style.color = "white";
        priceText.remove();
        let clouse = document.querySelector(".clouse");
        clouse.addEventListener("click", () => {
            span.remove();
            priceInput.value = "";
        });
    }

});